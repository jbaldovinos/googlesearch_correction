from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait


class GoogleHomePage():
    searchbox_name = "q"
    logo_google_xpath = "//div[@class='logo doodle']//a//img"
    result_stats_xpath = "//div[@id='result-stats']"
    correction_message_xpath = "//a[@class='gL9Hy']"


    def __init__(self, driver):
        self.driver = driver
        self.search_validation = None
        self.title = []
        self.validate_search_Xpath = "//input[@class='gLFyf gsfi']"

    # This function get the word form the console and send it to google search box
    def sendInput(self, wordSearch):
        try:
            self.driver.find_element_by_name(self.searchbox_name).send_keys(wordSearch)
        except:
            print("The word is not on the search box")

    # This function enter the value to begin with the search
    def clickSearch(self):
        try:
            self.driver.find_element_by_name(self.searchbox_name).send_keys(Keys.ENTER)
        except:
            print("Search incomplete")

    # This function validates if the correction link is present in the result page
    def correctioSearch(self):
        try:
            wait = WebDriverWait(self.driver, 10)
            correctionLink = self.driver.find_element_by_xpath(self.correction_message_xpath)
            if correctionLink == True:
                print("ERROR")
                pass
            elif correctionLink == False:
                print("Instert a valid word")
        except:
            print("The correction link in not present in the page")

    #This method validate that at least one time the wordSearch is present on the title
    def validate_word(self):
        search_validation = (self.driver.find_elements_by_xpath(self.validate_search_Xpath))
        for x in self.title:
            if search_validation in x.text:
                return False
        return True
